# pub/sub topic creation

resource "google_pubsub_topic" "pubsub-topic" {
  name = var.topic
  depends_on = var.depends_on
# message_retention_duration = "86600s"   this much time message can reteain in a topic


}

# Create Pub/Sub Subscription (optional)

resource "google_pubsub_subscription" "subscription" {
  topic = google_pubsub_topic.pubsub-topic.name
  for_each = toset(var.sub)
  name = each.key

}

